import socket
from threading import Thread
import time
from distload.db import PeerDB
from sys import stderr
from Chains import Blockchain
from os import remove

class Connection():
    def __init__(self, sock):
        self.socket = sock
        self.open = True

    def sendRaw(self, *args, **kwargs):
        # Wrapper for the socket send call
        return self.socket.send(*args, **kwargs)

    def send(self, data, encoding="utf-8", timeout=5):
        # Send data with trailing \r\n\r\n
        self.socket.settimeout(timeout)
        return self.sendRaw(data.encode(encoding) + "\r\n\r\n".encode(encoding))

    def recvRaw(self, size=1024):
        # Wrapper for the socket recv call
        self.socket.settimeout(1)
        return self.socket.recv(size)

    def recv(self, encoding="utf-8", timeout=5):
        self.sock.settimeout(timeout)
        starttime = time.time()
        fulldata = ""
        data = self.recvRaw().decode(encoding)
        while data[-4:] != "\r\n\r\n":
            tout = timeout - (time.time() - starttime)
            if tout <= 0:
                raise socket.timeout
            fulldata += data
            data = self.recvRaw().decode(encoding)
        fulldata += data[:-4]
        return fulldata

    def close(self):
        self.socket.close()
        self.open = False

class Client:
    def __init__(self, host, port, encoding="utf-8"):
        self.sock = Connection(socket.socket())
        self.sock.socket.connect((host, port))
        self.encoding = encoding

    def sendRequest(self, request, **kwargs):
        return self.sock.send(request, encoding=self.encoding, **kwargs)

    def recv(self, **kwargs):
        return self.sock.recv(encoding=self.encoding, **kwargs)

    def getBlock(self, blockid):
        self.sendRequest("g" + str(blockid))

    def pushBlock(self, blockid, blockdata):
        self.sendRequest("p" + str(blockid) + " " + str(blockdata))
    
    def close(self):
        self.sock.close()

    def __del__(self):
        self.close()

class Peer:
    def __init__(self, blockchain, peerfile):
        self.peerfile = peerfile
        self.blockchain = Blockchain

    def getPeerList(self):
        db = PeerDB(self.peerfile)
        peercount = db.size()
        peerlist = []
        i = 0 
        while i < peercount:
            peerlist.append(db.read(i))
            i += 1

        return peerlist

    def pushBlock(self, blockid):
        block = Blockchain(self.blockchain).getBlock(blockid) 
        peerlist = self.getPeerList()
        for peer in peerlist:
            try:
                cl = Client(peer, 4477)
                cl.pushBlock(blockid, str(block))
                cl.close()
            except Exception as e:
                print("Unable to push block", blockid, file=stderr)
                continue

    def getBlock(self, blockid, tmpsuffix=".tmp"):
        peerlist = self.getPeerList()
        for peer in peerlist:
            try:
                cl = Client(peer, 4477)
                cl.getBlock(blockid)
                data = cl.recv()
                cl.close()
                with open(self.blockchain, "r") as reader:
                    with open(self.blockchain + tmpsuffix, "w") as f:
                        f.write(reader.read())
                bc = Blockchain(self.blockchain + tmpsuffix)
                bc.insertBlock(blockid, data)
                try:
                    bc.check()
                    with open(self.blockchain + tmpsuffix) as reader:
                        with open(self.blockchain) as f:
                            f.write(reader.read())
                    remove(self.blockchain + tmpsuffix)
                    return 
                except Exception as e:
                    print("Error in downloaded blockchain:", e)
                    continue 
            except:
                print("[*] Unable to download block from " + str(peer), file=stderr)
                continue
        raise IndexError("No valid peers")
