from ipaddress import IPv4Address
from os import stat

class PeerAddress(IPv4Address):
    pass

class PeerDB:
    def __init__( self, filename ):
        self.path = filename

    def open( self, mode="rb" ):
        return open( self.path, mode )

    def openat( self, index, **kwargs ):
        f = self.open( **kwargs )
        f.seek( index * 4 )
        return f

    def size( self ):
        statinfo = stat(self.path)
        return statinfo.st_size / 4

    def read( self, index ):
        f = self.openat( index, mode="rb" )
        addrdata = f.read( 4 )
        f.close()
        if not addrdata or len(addrdata) != 4:
            return False
        return PeerAddress(addrdata)

    def write( self, addr ):
        # Check if address is already written to file
        try:
            if self.find(addr):
                return
        except FileNotFoundError:
            pass

        # Parse IP address
        dat = PeerAddress(addr).packed

        # Write to file
        f = self.open( mode="ab" )
        f.write( dat )
        f.close()

    def replace( self, index, addr ):
        writer = self.openat( index, mode="r+b" )
        writer.write( PeerAddress(addr).packed )
        writer.close()

    def find( self, addr ):
        addrdat = PeerAddress( addr ).packed

        reader = self.open( mode="rb" )

        dat = False
        i = 1
        while True:
            dat = reader.read( 4 )
            if not dat:
                break
            if dat == addrdat:
                reader.close()
                return i
            i += 1

        reader.close()
        return False
