from Chains.blockchain import Blockchain, Block
from time import time
from distload.db import PeerDB
from sys import stderr
import socket
from threading import Thread

def inf(*args, **kwargs):
    print(*args, file=stderr, **kwargs)

def handleCBDPConnection(blockchain, client, addr):
    session = CBDPSession(blockchain, client, addr)
    session.handle()

def asyncHandleCBDPConnection(blockchain, client, addr):
    t = Thread(target=handleCBDPConnection, args=(blockchain, client, addr))
    t.start()
    return t

class Server:
    def __init__(self, port=4477):
        self.port = port
        self.server = self.getServer()
    
    def getServer(self):
        sock = socket.socket() 
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(("0.0.0.0", self.port))
        sock.listen(5)
        return sock

    def logConnection(self, address):
        inf("[*] Connection from", address[0])

    def getConnection(self):
        return self.server.accept()

    def acceptClient(self):
        client, addr = self.getConnection()
        self.logConnection(addr)
        return client, addr

    def mapSessions(self, blockchain, func):
        while True:
            client, addr = self.acceptClient()
            func(blockchain, client, addr)

    def serve(self, blockchain):
        self.mapSessions(blockchain, asyncHandleCBDPConnection)

class CBDPSession:
    def __init__(self, blockchain, sock, addr):
        self.sock = sock
        self.sock.settimeout(2)
        self.blockchain = blockchain
        self.addr = addr

    def getRequestType(self):
        try:
            requestByte = self.sock.recv(1)
        except Exception as e:
            self.logError("Unable to receive request byte:", e)
            return False

        if not requestByte:
            self.logError("Unable to receive requestByte")
            return False

        if requestByte == b"g":
            return "get"
        elif requestByte == b"p":
            return "push"
        elif requestByte == b"c":
            return "count"
        else:
            self.logError("Received invalid request type:", requestByte)
            return False

    def logError(self, *message):
        inf("[ERROR] ", end="")
        inf(*message)

    def logInfo(self, *message):
        inf("[INFO] ", end="")
        inf(*message)

    def recvBlockID(self):
        try:
            dat = self.sock.recv(4)
        except:
            return False
        
        if len(dat) != 4:
            return False

        blockid = int.from_bytes(dat, "little")
        return blockid

    def getBlockID(self):
        blockid = self.recvBlockID()
        if blockid == False or blockid < 0:
            self.logError("Unable to download block id")
            return False
        return blockid

    def handleCountRequest(self):
        bc = Blockchain(self.blockchain)
        try:
            linecount = bc.countLines()
        except:
            self.logError("Unable to count lines of blockchain")
            return False
        try:
            sentlen = self.sock.send(linecount.to_bytes(4, "little"))
            if sentlen != 4:
                return False
        except:
            return False
        return True
    
    def getBlock(self, blockid): 
        bc = Blockchain(self.blockchain)
        try:
            block = bc.getBlock(blockid)
            if not block:
                raise Exception
        except:
            return False
        return block

    def sendBlock(self, block): 
        blocknotation = str(block) + "\n"
        blockdat = blocknotation.encode("utf-8")
        ret = self.sock.send(blockdat)
        if ret != len(blockdat) or ret <= 0:
            self.logError("Unable to send", len(blockdat), "bytes to server ( only", ret, ")")
            return False
        self.logInfo("Sent", ret, "bytes of data")
        return ret

    def handleGetRequest(self):
        blockid = self.getBlockID()
        if blockid == False:
            return False
        
        bc = Blockchain(self.blockchain)
        try:
            block = bc.getBlock(blockid)
            if block == False:
                raise Exception
        except:
            self.logError("Unable to read requested block (", blockid, ") from file")
            return False

        self.sendBlock(block)
        return True

    def handlePushRequest(self):
        blockid = self.getBlockID()
        if blockid == False:
            return False
        self.logInfo("Client pushed block", blockid)
        bc = Blockchain(self.blockchain)
        lc = bc.countLines()
        if lc > blockid:
            self.logInfo("Block already there")
            return False
        return True

    def handleRequestType(self, requestType):
        if requestType == "count":
            return self.handleCountRequest()
        elif requestType == "get":
            return self.handleGetRequest()
        elif requestType == "push":
            return self.handlePushRequest()

    def handle(self):
        requestType = self.getRequestType()
        if not requestType:
            self.logError("Unable to receive request type")
            self.sock.close()
            return False

        if not self.handleRequestType(requestType):
            self.logError("Unable to handle request")
            self.sock.close()
            return False

        self.sock.close()
        self.logInfo("Finalized session.")
        return True

class Client:
    def __init__(self, host, port=4477):
        self.serverhost = host
        self.serverport = port
        self.sock = self.getConnection()
        self.sock.settimeout(2)

    def startTime(self):
        self.starttime = time()

    def isReached(self, timeout):
        if time() - self.starttime >= timeout: 
            return True
    
    def getConnection(self):
        sock = socket.socket()
        try:
            sock.connect((self.serverhost, self.serverport))
        except:
            raise ConnectionError("Unable to connect to " + self.serverhost + ":" + str(self.serverport))
        return sock

    def sendBlockID(self, blockid):
        blockdat = blockid.to_bytes(4, "little")
        try:
            ret = self.sock.send(blockdat)
        except:
            raise ConnectionError("Unable to receive data")

        if ret != 4:
            raise ConnectionError("Unable to send 4 bytes of data (block id)")

        return ret

    def sendGetRequest(self, blockid):  
        self.sock.send(b"g")
        self.sendBlockID(blockid)

    def getBlock(self):
        blockdat = b""
        self.startTime()
        while True:
            try:
                blockdat += self.sock.recv(1024)
            except:
                raise ConnectionError("Connection closed.")
            if not blockdat:
                raise ConnectionError("Connection closed.")
            if blockdat[len(blockdat) - 1] == 10: # newline
                break
            if self.isReached(2):
                raise ConnectionError("Connection timed out")
        blockdat.replace(b"\n",b"")
        block = False
        try:
            block = Block.fromString(blockdat.decode())
        except Exception as e:
            raise ConnectionError("Invalid block received:", e)
        return block

    def downloadBlock(self, blockid):
        self.sendGetRequest(blockid)
        return self.getBlock()

    def pushBlock(self, blockid):
        try:
            if self.sock.send(b"p") != 1:
                raise Exception
            if self.sock.send(blockid.to_bytes(4, "little")) != 4:
                raise Exception
        except:
            raise ConnectionError("Unable to send push request")
    
    def close(self):
        self.sock.close()
    
    def __del__(self):
        self.close()
