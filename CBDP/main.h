#ifndef CHAINS_MAIN_H_
#define CHAINS_MAIN_H_

void fatal(char *msg, ...);
void info(char *msg, ...);
void error(char *msg, ...);
void error_errno();
void fatal_errno();
void success(char *msg, ...);
void warn(char *msg, ...);

#endif
