#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

void fatal(char *msg, ...) {
	va_list args;
	va_start(args, msg);
	fprintf(stderr, "[\033[31mERROR\033[0m]\t\t");
	vfprintf(stderr, msg, args);
	fprintf(stderr, "\n");
	va_end(args);
	exit(1);
}

void warn(char *msg, ...) {
	va_list args;
	va_start(args, msg);
	fprintf(stderr, "[\033[33mWARNING\033[0m]\t");
	vfprintf(stderr, msg, args);
	fprintf(stderr, "\n");
	va_end(args);
}	
void info(char *msg, ...) {
	va_list args;
	va_start(args, msg);
	fprintf(stderr, "[\033[36mINFO\033[0m]\t\t");
	vfprintf(stderr, msg, args);
	fprintf(stderr, "\n");
	va_end(args);
}

void error(char *msg, ...) {
	va_list args;
	va_start(args, msg);
	fprintf(stderr, "[\033[31mERROR\033[0m]\t\t");
	vfprintf(stderr, msg, args);
	fprintf(stderr, "\n");
	va_end(args);
}

void success(char *msg, ...) {
	va_list args;
	va_start(args, msg);
	fprintf(stderr, "[\033[32mSUCCESS\033[0m]\t");
	vfprintf(stderr, msg, args);
	fprintf(stderr, "\n");
	va_end(args);
}

void error_errno() {
	error("%s", strerror(errno));
}

void fatal_errno() {
	fatal("%s", strerror(errno));
}
