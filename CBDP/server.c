#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>

#include "main.h"

#define LOCK_FILE "bc.lock"

#define SECCHECK_SCRIPT "/usr/share/chains/cli/shells/net/seccheck key"

#define TIMEOUT_SECS 1
#define TIMEOUT_USECS 0

int server_sock(int port, int queue) {
	int sock;
	const int yes = 1, size = sizeof(int);
	struct sockaddr_in local;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == -1)
		return -1;

	if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, size) == -1) {
		close(sock);
		return -1;
	}

	local.sin_addr.s_addr = 0;
	local.sin_port = htons(port);
	local.sin_family = AF_INET;
	bzero(local.sin_zero, sizeof(local.sin_zero));
	if(bind(sock, (struct sockaddr *) &local,
		      sizeof(local)) == -1) {
		close(sock);
		return -1;
	}

	if(listen(sock, queue) == -1) {
		close(sock);
		return -1;
	}
	return sock;
}

struct handlerparam {
	int hp_sock;
	void *hp_param;
};

typedef char bool;

#define true 1
#define false 0

bool addrinfile(in_addr_t ad, char *filename) {
	FILE *reader;
	char buffer[100];
	reader = fopen(filename, "r");
	if(reader == NULL)
		return false;
	while(!(feof(reader))) {
		bzero(buffer, strlen(buffer));
		fgets(buffer, 100, reader);
		if(ad == inet_addr(buffer))
			return true;
	}
	fclose(reader);
	return false;
}

void server_handle(int sock, void *(*func) (void *), void *param,
                   char *hostsfile) {
	pthread_t thread;
	int client;
	struct sockaddr_in remote;
	int size = sizeof(remote);
	struct handlerparam *hp;
	FILE *writer;

	info("Waiting for connection...");
	while(1) {
		client = accept(sock, (struct sockaddr *) &remote, &size);
		if(client == -1) {
			error_errno();
			continue;
		}
		success("Connection from %s:%d", inet_ntoa(remote.sin_addr),
		       			      	 ntohs(remote.sin_port));
		if(hostsfile != NULL) {
			if(addrinfile(remote.sin_addr.s_addr, hostsfile)) {
				info("Address already saved");
			} else {
				writer = fopen(hostsfile, "a");
				if(writer == NULL) {
					error("Unable to open hosts file");
					error_errno();
				}
				else {
					fprintf(writer, "%s\n", inet_ntoa(remote.sin_addr));
					fclose(writer);
				}
			}
			hp = malloc(sizeof(struct handlerparam));
			if(hp == NULL) {
				error("Unable to allocate space for parameter");
				close(client);
				continue;
			}
			hp->hp_sock = client;
			hp->hp_param = param;
			pthread_create(&thread, NULL, func, (void *) hp);
		}
	}
}

void getlock() {
	FILE *locker;
	locker = NULL;
	while(locker == NULL) {
		while(access(LOCK_FILE, F_OK) != -1) {
			sleep(0.1);
		}
		locker = fopen(LOCK_FILE, "w");
	}
	fclose(locker);
}
void endlock() {
	remove(LOCK_FILE);
}

void *cbdp_handle(void *sockptr) {
	struct handlerparam *hp = ((struct handlerparam *)sockptr);

	int sock = hp->hp_sock, ret, len, i;

	unsigned long long bid;
	struct timeval timeout;
	char *buffer, c;
	time_t start_time, end_time;
	double timediff = (double) 0;

	char *filename = (char *) hp->hp_param;

	FILE *file;

	timeout.tv_sec = TIMEOUT_SECS;
	timeout.tv_usec = TIMEOUT_USECS;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
	setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout));

	buffer = malloc(1024);
	if(buffer == NULL) {
		error("Unable to allocate heap memory in thread %u", pthread_self());
		error_errno();
		free(sockptr);
		return NULL;
	}
	bzero(buffer, 1024);

	len = strlen(buffer);
	while(1) {
		time(&start_time);
		ret = recv(sock, buffer + len, 1024 - len, 0);
		len = strlen(buffer);
		if(len > 4) {
			if(strcmp(buffer + len - 4, "\r\n\r\n") == 0) {
				break;
			}
		}
		if(ret == -1) {
			error("Unable to receive request in thread %u", pthread_self());
			error_errno();
			free(buffer);
			close(sock);
			free(sockptr);
			return NULL;
		}
		time(&end_time);
		timediff += difftime(end_time, start_time);
		if(timediff > (double) 2) {
			error("Client timed out");
			close(sock);
			free(buffer);
			free(sockptr);
			return NULL;
		}
	}

	getlock();

	if(buffer[0] == 'g') {
		bid = strtoull(buffer + 1, NULL, 10);
		if(errno == ERANGE) {
			error("Invalid requested number: %s", buffer);
			strcpy(buffer, "ERR SYNTAX\r\n\r\n");
			send(sock, buffer, strlen(buffer), 0);
			close(sock);
			free(buffer);
			free(sockptr);
			endlock();
			return NULL;
		}
		file = fopen(filename, "r");
		if(file == NULL) {
			error("Unable to open file");
			error_errno();
			bzero(buffer, strlen(buffer));
			strcpy(buffer, "ERR LOCAL\r\n\r\n");
			send(sock, buffer, strlen(buffer), 0);
			close(sock);
			free(buffer);
			free(sockptr);
			endlock();
			return NULL;
		}
		for(i = 1; i < bid; i++) {
			while((c != '\n') && (c != EOF)) {
				c = fgetc(file);
			}
			c = 0;
			if(feof(file)) {
				bzero(buffer, strlen(buffer));
				strcpy(buffer, "ERR EOF\r\n\r\n");
				send(sock, buffer, strlen(buffer), 0);
				free(buffer);
				close(sock);
				free(sockptr);
				fclose(file);
				endlock();
				return NULL;
			}
		}
		do {
			i = 0;
			bzero(buffer, strlen(buffer));
			while((buffer[i - 1] != '\n') && (i < 1024)) {
				buffer[c] = fgetc(file);
				i++;
			}
			send(sock, buffer, strlen(buffer), 0);
			if(feof(file))
				break;
		} while(i >= 1024);
		fclose(file);
	} else if(buffer[0] == 'p') {
		char buf[1024], *ptr;
		ptr = strstr(buffer, " ");
		if(ptr == NULL) {
			free(sockptr);
			strcpy(buffer, "ERR SYNTAX\r\n\r\n");
			send(sock, buffer, strlen(buffer), 0);
			free(buffer);
			close(sock);
			endlock();
			return NULL;
		}
		strncpy(buf, buffer + 1, ptr - (buffer + 1));
		bid = strtoull(buf, NULL, 10);
		strncpy(buf, ptr + 1, strstr(buffer, "\r\n\r\n") - ptr);
		bzero(buffer, strlen(buffer));
		sprintf(buffer, "%s \"%s\" \"%s\" %d", SECCHECK_SCRIPT, filename, buffer, bid);
		info("Running %s", buffer);
		ret = system(buffer);
		if(ret) {
			strcpy(buffer, "ERR SEC");
			send(sock, buffer, strlen(buffer), 0);
			error("Seccheck failed.");
		} else {
			success("Committing changes");
		}
	}

	send(sock, "\r\n\r\n", 4, 0);
	close(sock);
	free(buffer);
	free(sockptr);
	endlock();
	return NULL;
}

void server_handlecbdp(int sock, char *filename, char *hostsfile) {
	server_handle(sock, cbdp_handle, (void *) filename, hostsfile);
}

int server_cbdp(char *filename, char *hostsfile, int port, int queue) {
	int sock;
	sock = server_sock(port, queue);
	if(sock == -1) {
		error("Unable to create server socket");
		error_errno();
		return -1;
	}
	success("Listenning on localhost:%d", port);
	server_handlecbdp(sock, filename, hostsfile);
	return 0;
}
