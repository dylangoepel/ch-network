#ifndef CHAINS_SERVER_H_
#define CHAINS_SERVER_H_

int server_sock(int port, int queue);
void server_handle(int sock, void (*func) (void *), void *, char *);
void *cbdp_handle(void *);
void server_handlecbdp(int sock, char *, char *);
void server_cbdp(char *, char *, int, int);
struct handlerparam;

#endif
